# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
export EDITOR=nvim
export VISUAL=nvim
autoload -Uz compinit promptinit
compinit
promptinit
prompt_redhatcolor_setup()
{
	PS1="%B[%b%F{green}%n%f@%F{yellow}%m%f %F{blue}%B%~%b%f%B]%b%F{red}%(!.#.$)%f "
}
prompt_themes+=(redhatcolor)

prompt redhatcolor

# The next line updates PATH for client-keystone-auth.
if [ -f '/home/slava//vk-cloud-solutions/path.bash.inc' ]; then source '/home/slava//vk-cloud-solutions/path.bash.inc'; fi

