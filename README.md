# Деплой личных конфигов
Ansible playbook написанный для деплоя личных конфиг-файлов
## Инструкция
1. Клонировать репозиторий
'git clone https://gitlab.com/chelobos/chelobos-arch-config-files.git'
2. Установить ansible-core
'pacman -S ansible-core'
3. Запустить playbook
'ansible-playbook play.yaml'
4. Заполнить все промпты
## Лицензия
GPLv2 или более поздняя
